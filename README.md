# Jade to PHP テスト（Wocker）

## 必要な環境

* gulp (Node, npm)
* wocker (Vagrant)


## テーマのインストール

`wp-content/themes/` の中に置く。


## gulpパッケージのインストール

ターミナルでテーマのディレクトリに移動する。


以下を実行。

```bash
npm install
```


## wocker.devが動く状態にしておく

別ターミナルでwockerを起動し、テーマを有効化。
`wocker.dev` でサイトが見られる状態にしておく。


## gulp実行

```bash
gulp
```

タスクが実行されて、ブラウザの新規タブで `localhost:3000` が立ち上がるはずです。


## 編集してみる


`.scss` ファイル、`.jade` ファイルを編集すると、自動コンパイル＆自動リロードが行われます。