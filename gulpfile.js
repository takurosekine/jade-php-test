var gulp         = require("gulp");
var browserSync  = require("browser-sync");
var autoprefixer = require('gulp-autoprefixer');
var cssnano      = require("gulp-cssnano");
var jade         = require('gulp-jade-php');
var plumber      = require('gulp-plumber');
var rename       = require('gulp-rename');
var sass         = require('gulp-sass');

// Broswer for Autoprefixer
var supportBrowser = [
	'last 5 version',
	'ie >= 10',
	'ie_mob >= 10',
	'ff >= 30',
	'chrome >= 34',
	'safari >= 7',
	'opera >= 23',
	'ios >= 7',
	'android >= 4.2',
];

var path = {
	root : './',
	css  : './assets/css',
	scss : './assets/css/sass',
	jade : './assets/jade'
};

// Sass
gulp.task('sass', function() {
	return gulp.src( path.scss + '/**/*.scss' )
		.pipe(plumber())
		.pipe(sass())
		.pipe(autoprefixer({
			browsers: supportBrowser,
			cascade: false
		}))
		.pipe(gulp.dest( path.css ))
		.pipe(rename({suffix: '.min'}))
		.pipe(cssnano())
		.pipe(gulp.dest( path.css ))
});

// Jade to PHP
gulp.task('jadePHP', function() {
	return gulp.src([
			path.jade + '/*.jade',
			'!' + path.jade + '/**/_include/**.jade',
			'!' + path.jade + '_*.jade',
		])
		.pipe(plumber())
		.pipe(jade({
			pretty: true,
			basedir: path.jade
		}))
		.pipe(gulp.dest( path.root ));
});

// Browser Sync
gulp.task('serve', function() {
	browserSync({
		proxy: 'wocker.dev',
		notify: false,
	});
});

// Watch
gulp.task('watch', ['serve'], function () {
	gulp.watch([ path.jade + '/**/*.jade' ], ['jadePHP']); // jade更新でコンパイル
	gulp.watch([ path.scss + '/**/*.scss' ], ['sass']); // sass更新でコンパイル
	gulp.watch([ 
		path.root + '/**/*.php',
		path.css + '/**/*.min.css'
	]).on('change', browserSync.reload); // .php か .min.css の更新でリロード
});

// Task
gulp.task('default', ['watch']);