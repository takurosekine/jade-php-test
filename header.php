<!DOCTYPE html><html lang="<?php language_attributes(); ?>">
<head>
  <title><?php wp_title( " | ", true, "right" ); ?>
  </title><?php if(is_home() || is_front_page()): ?><meta name="description" content="<?php bloginfo( 'description' ); ?>">
  <meta itemprop="description" content="<?php bloginfo( 'description' ); ?>">
  <meta name="keywords" content=""><?php endif; ?><link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/style.min.css"><?php wp_head(); ?>
</head><body id="top" <?php body_class(); ?>>
<header>
  <h1>Example</h1>
  <ul>
    <li><a href="#">header link</a></li>
    <li><a href="#">header link</a></li>
    <li><a href="#">header link</a></li>
    <li><a href="#">header link</a></li>
  </ul>
</header>